[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.lfo.tu-dortmund.de%2Fameyer%2Fintro_python_for_time_series/HEAD)

# Intro Python For Time Series

## Getting started

This is a repo introducing some very basic Python syntax and some libraries
to show how to quickly get along with time series data.

## Authors and acknowledgment

Anne Meyer
