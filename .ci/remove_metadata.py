import nbformat
import argparse

def remove_metadata(notebook_path):
    nb = nbformat.read(notebook_path, as_version=nbformat.NO_CONVERT)

    # Remove all metadata fields
    nb.metadata = {}

    nbformat.write(nb, notebook_path, version=nbformat.NO_CONVERT)
    
    
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("function", choices=["remove_metadata"], help="Function to run")
    parser.add_argument("--file", help="file path")

    args = parser.parse_args()

    if args.function == "remove_metadata":
        remove_metadata(args.file)
    else:
        pass