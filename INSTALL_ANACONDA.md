![1](images/1.png)

# Getting started with Anaconda and Jupyter Notebooks

If you're new to Anaconda and Jupyter Notebooks, here's a simple guide on how to install them on your personal computer:

## What is Anaconda?

Anaconda is a free, open source distribution of Python (and R) for data analytics
applications. What does that mean? It already contains many useful libraries (e.g. for reading
data, for visualizing data, for machine learning) for data analytics tasks and it provides a
package management tool. A package management tool is used to easily install new
packages with just one line of code (see below). This tool is called conda. Python has its own
package management system called pip. Usually, conda should be fine.

Furthermore, anaconda already contains development environments such as Spyder,
JupyterLab and Jupyter Notebook. We will use Jupyter Notebook for learning Python (see
below).

Anaconda is available for Window, Linux and MacOS.

## What is Jupyter Notebook?

Jupyter Notebook is an interactive web environment to write, execute and comment Python
code, which you can use in your web browser:

![2](images/2.png)

A notebook does not only contain (live) code, but you can also show the data and charts you
are working with. To run the code of a cell, you just have to press the “Run” button on top.
You can also use “Ctrl” + ”Enter” to run your code cell.
Furthermore, you can add a documentation using the “markdown” language by changing the
cell type to “Markdown” (see Menu “Cell”): Define for example a header by using “# My first
header” or “## My second header” or an enumeration by using “* my first line”. Press the
button “Run” to show the markdown of the cell as pretty text and double click the cell to
change the markdown.
Jupyter notebooks are a nice environment for exploring your data with tables and charts,
document your insights and share it with a colleague or a customer. Furthermore, it is really
helpful for learning Python.

For larger projects, you should use a development environment such as PyCharm.

## Installing Anaconda (Windows 10)

**The instructions and illustration depict the installation process of an older Anaconda version (Python 3.9). However, please note that despite this, the installation should function using the same procedure. So, there's no need for confusion.**

If you have a MacOS or Linux machine, you can contact us for help!

![3](images/3.png)

1. Download Anaconda for your operating system from the official Anaconda website:
<https://www.anaconda.com/products/distribution>

    **Be careful to download the Python 3 version (currently Python 3.10).**
    Depending on System, download the 64 or 32-Bit Installer. Check out your system type:
    System information -> System type (in German: Systeminformation -> Systemtype)

    ![4](images/4.png)

2. Open the downloaded file and press on “Next”

    ![5](images/5.png)

3. Check out the license agreement and press on agree, and then choose whether to install
Anaconda just for your User or for all User

    ![6](images/6.png)

4. Press on “Next”

    ![7](images/7.png)

5. Finally check the second box as shown in the image below, and then click on “Install”

    ![8](images/8.png)

## Starting a Jupyter Notebook

With the successful installation of Anaconda you should be able to find jupyter notebook in
your programs: Press Windowskey -> type “jupyter notebook” and open it.
You will be spawned in a new tab of your default Browser that has opened your Document
Folder (C:\Users\YOURNAME\Documents). Furthermore, a console is opened.

However if this is not the case: Open Anaconda Prompt (Windowskey -> type “Anaconda prompt”) and type in “jupyter-notebook” or “jupyter notebook

![9](images/9.png)

You should see something like that ...

![10](images/10.png)

It is a good idea, to keep all your notebooks together in a folder. Create a folder called e.g.
“C:\Users\YOURNAME\Documents\Notebooks”.
If you want to look to a notebook of somebody else, just copy the notebook file to your “Notebooks” folder and it will appear in the overview in your web browser. Just click to open it!

If you want to change the start-up folder, that means, the folder in which your jupyter
notebook web overview starts and where your new notebooks are stored
(e.g. in “C:\Projekte\Notebooks”), check out this tutorial:

<https://jupyter-notebook-beginner-guide.readthedocs.io/en/latest/execute.html#change-jupyter-notebook-startup-folder-windows>

Alternatively, you can enter the data path in your Anaconda Prompt command line and open
Jupyter Notebook from this prompt. Use “cd” to change directory and “jupyter notebook” to
open Jupyter Notebook.

Example (type the commands after enter on the right side):

``` bash
(base) C:Windows\system32> enter: cd \
(base) C:\> enter: cd Users
(base) C:\Users> enter: cd USERNAME
(base) C:\Users\USERNAME> enter: cd Documents
(base) C:\Users\USERNAME\Documents enter: cd Notebooks
(base) C:\Users\USERNAME\Documents\Notebooks enter: jupyter notebook
```

Instead of changing every folder with one statement you can enter in one line:

``` bash
cd Users\USERNAME\Documents\Notebooks
jupyter notebook
```

Your default browser will open automatically, showing the folder you defined.

## Getting Started: Downloading Your First Jupyter Notebook Tutorial

We have provided a first Jupyter notebook for download at the [following link](https://asca-mqtt.lfo.tu-dortmund.de/kit_ils_day0.zip). Please unzip the provided zipfile. It contains a notebook that explains the basic usage of Jupyter notebooks and serves as a test for the installation you just performed.

To begin, copy the file *"TUT_0_my_first_jupyter_notebook.ipynb"* to your *"Notebooks"* folder or utilize an alternative method. Launch the notebook and thoroughly read its contents. Engage with the provided examples to gain your initial hands-on experience with Jupyter notebooks.

In case you encounter any issues, we kindly request you to promptly reach out for assistance. It is recommended to seek help at the earliest opportunity.
